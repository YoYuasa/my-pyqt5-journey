from PyQt5.QtCore import QSize,Qt
from PyQt5.QtWidgets import QApplication,QMainWindow,QPushButton

# Only needed for access to command line tools
import sys

# Subclass QMainWindow to customize your application's main window
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("My App")
        self.button = QPushButton("Press Me!")
        self.button.clicked.connect(self.the_button_was_clicked)
        # Set the central widget of the window
        self.setCentralWidget(self.button)
    def the_button_was_clicked(self):
        self.button.setText("You already clicked me.")
        self.button.setEnabled(False)
        self.setWindowTitle("My Oneshot App")
# You need one (and only one) QApplication Instance per application
# Pass in sys.argv to allow command line arguments for your app
app = QApplication(sys.argv)

# Create a QtWidget, which will be our window
window = MainWindow()
window.show()  # IMPORTANT!! Windows are hidden by default

# Start the event loop
app.exec_()

# Your application wont reach here until you exit
# and the event loop has stopped
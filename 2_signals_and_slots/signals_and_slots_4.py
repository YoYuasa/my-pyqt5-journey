from PyQt5.QtCore import QSize,Qt
from PyQt5.QtWidgets import QWidget , QApplication, QLabel, QLineEdit, QMainWindow, QPushButton, QWidget, QVBoxLayout, QWidget

# Only needed for access to command line tools
import sys
from random import choice

window_titles = [
    'My App',
    'My App',
    'Still My App',
    'Still My App',
    'What on earth!',
    'What on earth!',
    'This is surprising',
    'This is surprising',
    'Something went wrong'
]

# Subclass QMainWindow to customize your application's main window
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("My App")
        self.label = QLabel()
        self.input = QLineEdit()
        self.input.textChanged.connect(self.label.setText)
        layout = QVBoxLayout()
        layout.addWidget(self.input)
        layout.addWidget(self.label)
        container = QWidget()
        container.setLayout(layout)
        # Set the central widget of the window 
        self.setCentralWidget(container)

# You need one (and only one) QApplication Instance per application
# Pass in sys.argv to allow command line arguments for your app
app = QApplication(sys.argv)

# Create a QtWidget, which will be our window
window = MainWindow()
window.show()  # IMPORTANT!! Windows are hidden by default

# Start the event loop
app.exec_()

# Your application wont reach here until you exit
# and the event loop has stopped
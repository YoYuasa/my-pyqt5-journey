from PyQt5.QtWidgets import QApplication,QPushButton

# Only needed for access to command line tools
import sys

# You need one (and only one) QApplication Instance per application
# Pass in sys.argv to allow command line arguments for your app
app = QApplication(sys.argv)

# Create a QtWidget, which will be our window
window = QPushButton("Push Me!")
window.show()  # IMPORTANT!! Windows are hidden by default

# Start the event loop
app.exec_()

# Your application wont reach here until you exit
# and the event loop has stopped
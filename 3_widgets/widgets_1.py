import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("My App")

        widget = QLabel("Hello")
        font = widget.font()
        font.setPointSize(200)
        widget.setFont(font)
        # set text alignment to center of window
        widget.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        # can also do it like this
        widget.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(widget)

app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec_()